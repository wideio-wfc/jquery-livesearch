/******************************************************************************
* |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
*                          ..-.:.:...
*                       :.-- -     ..:...
*                   :.:. -             -.:...
*               :.:. -                     ..:...
*           :.:. .            _;:__.          ...-...
*       :.:. .               :;    -+_    -|       .--...
*   :.-- .                    -=      -~-.-           -.:...
* -:...              ___.      -=_                        -.--
* ...    .          =;  --=_     :=                   ..   ...
* .-.      . .              ~-___=;               . -      .:.
* ...           -.                             -.          ...
* .:.                .                    . .              .:.
* ...                   -.             -.                  ...
* .:.                      ...    . -                -~4>  .:.
* ...       _^+_.              -.                       2  ...
* .:.           ~,              .                 /'   _(  .:.
* ....           <              -          +'  ^LJ>   _^   ...
* .:..          _);             -     _   J   _/  ~~-'    .:.
* ....        _&i^i             .   _~_, <(   .^           ...
*  :.       _v>^  <             .  _X~'  -s,               .:.
*  :.             -=            .   S      ^'              ...
*  :....           -=_  ,       .   2                    ..-.:
*     :....          -^^        .                    .-.:. .
*        ..:...                 .                 ..:. -
*            -.:...             .           . :.-- -
*                 :....         .         -.-- .
*                    -.:...     .    ..:.: -
*                         -.:......--. -
*                             -.:. .
* 
* Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
* http://wide.io/
* ----------------------------------------------------------------------
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License.
*     
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*     
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see http://www.gnu.org/licenses/
* This work is released under GPL v 3.0
* ----------------------------------------------------------------------
* For all information : copyrights@wide.io
* ----------------------------------------------------------------------
* 
* |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
******************************************************************************/
/***
@title:
Live Search

@version:
2.0

@author:
Andreas Lagerkvist

@date:
2008-08-31

@url:
http://andreaslagerkvist.com/jquery/live-search/

@license:
http://creativecommons.org/licenses/by/3.0/

@copyright:
2008 Andreas Lagerkvist (andreaslagerkvist.com)

@requires:
jquery, jquery.liveSearch.css

@does:
Use this plug-in to turn a normal form-input in to a live ajax search widget. The plug-in displays any HTML you like in the results and the search-results are updated live as the user types.

@howto:
jQuery('#q').liveSearch({url: '/ajax/search.php?q='}); would add the live-search container next to the input#q element and fill it with the contents of /ajax/search.php?q=THE-INPUTS-VALUE onkeyup of the input.

@exampleHTML:
<form method="post" action="/search/">

	<p>
		<label>
			Enter search terms<br />
			<input type="text" name="q" />
		</label> <input type="submit" value="Go" />
	</p>

</form>

@exampleJS:
jQuery('#jquery-live-search-example input[name="q"]').liveSearch({url: Router.urlForModule('SearchResults') + '&q='});
***/
jQuery.fn.liveSearch = function (conf) {
	var config = jQuery.extend({
		url:			'/search-results.php?q=', 
		id:				'jquery-live-search', 
		duration:		400, 
		typeDelay:		200,
		loadingClass:	'loading', 
		onSlideUp:		function () {}, 
		uptadePosition:	false
	}, conf);

	var slideUp = function(element, ms, callback){
			if (!ms){ms = 400;}
			if (!callback){callback=function(){}}
			element.css({
				transition: ms+'ms ease all'
			});
			element.css({
				height: 0,
				paddingTop: 0,
				paddingBottom: 0
			});
			setTimeout(function(){
				element.hide();
				callback();
			}, ms);
			return element
		},
		slideDown = function(element, ms, callback){
			if (!ms){ms = 400;}
			if (!callback){callback=function(){}}
			element.css({
				transition: 'none'
			});
			var height = element.show().css({height:'',paddingTop:0,paddingBottom:0}).height(),
				padTop = element.css({paddingTop:'',paddingBottom:''}).css('padding-top'),
				padBottom = element.css('padding-bottom');
			element.css({
				height: 0,
				paddingTop: 0,
				paddingBottom: 0,
				display: 'block'
			});
			element.css({
				transition: ms+'ms ease all'});
			element.css({
				height: height,
				paddingTop: padTop,
				paddingBottom: padBottom
			});
			callback();
			return element
		};

	var liveSearch	= jQuery('#' + config.id);

	// Create live-search if it doesn't exist
	if (!liveSearch.length) {
		liveSearch = slideUp(jQuery('<div id="' + config.id + '"></div>')
						.appendTo(document.body)
						.hide(), 0);

		// Close live-search when clicking outside it
		jQuery(document.body).click(function(event) {
			var clicked = jQuery(event.target);

			if (!(clicked.is('#' + config.id) || clicked.parents('#' + config.id).length || clicked.is('input'))) {
				slideUp(liveSearch, config.duration, function () {
					config.onSlideUp();
				});
			}
		});
	}

	return this.each(function () {
		var input							= jQuery(this).attr('autocomplete', 'off');
		var liveSearchPaddingBorderHoriz	= parseInt(liveSearch.css('paddingLeft'), 10) + parseInt(liveSearch.css('paddingRight'), 10) + parseInt(liveSearch.css('borderLeftWidth'), 10) + parseInt(liveSearch.css('borderRightWidth'), 10);

		// Re calculates live search's position
		var repositionLiveSearch = function () {
			var tmpOffset	= input.offset();
			var inputDim	= {
				left:		tmpOffset.left, 
				top:		tmpOffset.top, 
				width:		input.outerWidth(), 
				height:		input.outerHeight()
			};
			var pos = 'fixed' ? input.parents().filter(function(){return $(this).css('position')=='fixed'}).length : 'absolute';

			inputDim.topPos		= inputDim.top + inputDim.height;
			inputDim.totalWidth	= inputDim.width; // - liveSearchPaddingBorderHoriz

			liveSearch.css({
				position:	pos,
				left:		inputDim.left + 'px',
				top:		inputDim.topPos + 'px',
				width:		inputDim.totalWidth + 'px'
			});
		};

		// Shows live-search for this input
		var showLiveSearch = function () {
			// Always reposition the live-search every time it is shown
			// in case user has resized browser-window or zoomed in or whatever
			repositionLiveSearch();

			// We need to bind a resize-event every time live search is shown
			// so it resizes based on the correct input element
			$(window).unbind('resize', repositionLiveSearch);
			$(window).bind('resize', repositionLiveSearch);

			slideDown(liveSearch, config.duration);
		};

		// Hides live-search for this input
		var hideLiveSearch = function () {
			slideUp(liveSearch, config.duration, function () {
				config.onSlideUp();
			});
		};

		$(this).on('url-change', function(){
			config.url = $(this).attr('data-livesearch-url');
			this.lastValue = "";
		});

		input
			// On focus, if the live-search is empty, perform an new search
			// If not, just slide it down. Only do this if there's something in the input
			.focus(function () {
				if (this.value !== '') {
					// Perform a new search if there are no search results
					if (liveSearch.html() == '') {
						this.lastValue = '';
						input.keyup();
					}
					// If there are search results show live search
					else {
						// HACK: In case search field changes width onfocus
						setTimeout(showLiveSearch, 1);
					}
				}
			})
			// Auto update live-search onkeyup
			.keyup(function () {
				// Don't update live-search if it's got the same value as last time
				if (this.value != this.lastValue) {
					if(this.value == "") {
						hideLiveSearch();
						return;
					} else {
						input.addClass(config.loadingClass);

						var q = this.value;

						// Stop previous ajax-request
						if (this.timer) {
							clearTimeout(this.timer);
						}

						// Start a new ajax-request in X ms
						this.timer = setTimeout(function () {
							var url = config.url;
							jQuery.get(url + q, function (data) {
								input.removeClass(config.loadingClass);

								// Show live-search if results and search-term aren't empty
								if (data.length && q.length) {
									liveSearch.html(data);
									showLiveSearch();
								}
								else {
									hideLiveSearch();
								}
							});
						}, config.typeDelay);
					}

					this.lastValue = this.value;
				}
			});
	});
};
